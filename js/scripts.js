/**
 * Manages the indeterminate checkboxes.
 *
 * @author Branden Ogata
 */

$(document).ready(function () {
  // The problem with indeterminate checkboxes is that both Firefox and Chrome seem to change the checked attribute
  // before the jQuery event handlers are called, making it difficult to use the state that was true when the user
  // clicked on the checkbox; we instead add a property to the indeterminate checkboxes to track their state
  $('.primary-checkbox').prop('state', 'no');

  $('#no-maybe-yes').change(function () {
    switch ($(this).prop('state')) {
      // If currently unchecked, change to indeterminate
      case 'no':
        $(this).prop('indeterminate', true);
        $(this).prop('state', 'indeterminate');
        break;

      // Else if currently indeterminate, change to checked
      case 'indeterminate':
        $(this).prop('indeterminate', false);
        $(this).prop('checked', true);
        $(this).prop('state', 'yes');

        $(this).next().children('input:checkbox').each(function () {
          $(this).prop('checked', true);
        });
        break;

      // Else (if currently checked), change to unchecked
      default:
        $(this).prop('checked', false);
        $(this).prop('state', 'no');

        $(this).next().children('input:checkbox').each(function () {
          $(this).prop('checked', false);
        });

        break;
    }
  });

  $('#no-yes-maybe').change(function () {
    console.log(`checked: ${$(this).prop('checked')}`);
    console.log(`indeterminate: ${$(this).prop('indeterminate')}`);

    switch ($(this).prop('state')) {
      // If currently unchecked, change to checked
      case 'no':
        $(this).prop('indeterminate', false);
        $(this).prop('checked', true);
        $(this).prop('state', 'yes');

        $(this).next().children('input:checkbox').each(function () {
          $(this).prop('checked', true);
        });

        break;
      // Else if currently checked, change to indeterminate
      case 'yes':
        $(this).prop('indeterminate', true);
        $(this).prop('checked', false);
        $(this).prop('state', 'indeterminate');
        break;

      // Else (if currently indeterminate), change to unchecked
      default:
        $(this).prop('checked', false);
        $(this).prop('state', 'no');

        $(this).next().children('input:checkbox').each(function () {
          $(this).prop('checked', false);
        });

        break;
    }
  });

  // Update primary checkboxes when sub-checkboxes change
  $('.sub-checkboxes > input:checkbox').change(function () {
    console.log($(this).parent().children('input:checkbox'));
    const primaryCheckbox = $(this).parent().prev();
    const selectedCheckboxes = $(this).parent().children('input:checkbox').filter((index, value) => {
      console.log(value);
      return $(value).prop('checked');
    }).length;
    const existingCheckboxes = $(this).parent().children('input:checkbox').length;

    console.log(`Selected checkboxes: ${selectedCheckboxes}`);
    console.log(`Existing checkboxes: ${existingCheckboxes}`);

    // If none of the checkboxes are selected, uncheck the primary checkbox
    if (selectedCheckboxes === 0) {
      $(primaryCheckbox).prop('checked', false);
      $(primaryCheckbox).prop('indeterminate', false);
      $(primaryCheckbox).prop('state', 'unchecked');
    }

    // Else if some (but not all) of the checkboxes are selected, set the checkbox to indeterminate
    else if (selectedCheckboxes < existingCheckboxes) {
      $(primaryCheckbox).prop('checked', false);
      $(primaryCheckbox).prop('indeterminate', true);
      $(primaryCheckbox).prop('state', 'indeterminate');
    }

    // Else (if all of the checkboxes are checked) check the primary checkbox
    else {
      $(primaryCheckbox).prop('checked', true);
      $(primaryCheckbox).prop('indeterminate', false);
      $(primaryCheckbox).prop('state', 'checked');
    }
  });
});
